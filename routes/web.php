<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/escolas');
});

Route::prefix('escolas')->group(function () {
    Route::get('/', 'SchoolController@index');
    Route::post('/', 'SchoolController@store');
    Route::get('/form', 'SchoolController@form');
    Route::get('/{idSchool}/delete', 'SchoolController@destroy');
    Route::get('/{idSchool}/edit', 'SchoolController@form');
    Route::post('/{idSchool}/edit', 'SchoolController@update');
    Route::get('/{idSchool}/turmas', 'SchoolClassController@index');
    Route::post('/{idSchool}/turmas', 'SchoolClassController@store');
    Route::get('/{idSchool}/turmas/form', 'SchoolClassController@form');
    Route::get('/{idSchool}/turmas/{idSchoolClass}/edit', 'SchoolClassController@form');
    Route::get('/{idSchool}/turmas/{idSchoolClass}/delete', 'SchoolClassController@destroy');
    Route::post('/{idSchool}/turmas/{idSchoolClass}/edit', 'SchoolClassController@update');

});

Route::prefix('alunos')->group(function () {
    Route::get('/', 'StudentController@index');
    Route::post('/', 'StudentController@store');
    Route::get('/form', 'StudentController@form');
    Route::get('/{idStudent}/delete', 'StudentController@destroy');
    Route::get('/{idStudent}/edit', 'StudentController@form');
    Route::post('/{idStudent}/edit', 'StudentController@update');
    Route::get('/{idStudent}/matriculas', 'EnrollController@index');
    Route::post('/{idStudent}/matriculas', 'EnrollController@store');
    Route::get('/{idStudent}/matriculas/form', 'EnrollController@form');
});

