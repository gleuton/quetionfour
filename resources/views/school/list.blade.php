@extends('layouts.main')
@section('title','Escola')

@section('content')
    <h1>Escolas: <a href="/escolas/form" class="btn btn-outline-success">Cadastrar</a></h1>
    <table class="table table-striped" id="schools">
        <thead class="table-primary">
        <tr>
            <th scope="col">Nome</th>
            <th scope="col">Endereço</th>
            <th scope="col">Opções</th>
        </tr>
        </thead>
        <tbody>
        @foreach($schools as $school)
            <tr>
                <td>{{$school->no_escola}}</td>
                <td>{{$school->endereco}}</td>
                <td>
                    <a href="/escolas/{{$school->id_escola}}/turmas" class="btn btn-outline-primary">Turmas</a>
                    <a href="/escolas/{{$school->id_escola}}/edit" class="btn btn-outline-secondary">Editar</a>
                    <a href="/escolas/{{$school->id_escola}}/delete" class="btn btn-outline-danger">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $schools->links() ?? ''}}
@endsection
