@extends('layouts.main')
@section('title','Escola')

@section('content')
    <h1>Escola:</h1>
    <form action="/escolas{{$school->edit ?? ''}}" method="post">
        @csrf
        <div class="form-group">
            <label for="noEscola">Nome da Escola:</label>
            <input type="text" class="form-control" name="no_escola" value="{{$school->no_escola ?? ''}}"
                   autocomplete="off" id="noscola" placeholder="Escola">
        </div>
        <div class="form-group">
            <label for="endereco">Endereço:</label>
            <input type="text" class="form-control" name="endereco" value="{{$school->endereco ?? ''}}"
                   autocomplete="off" id="endereco" placeholder="Endereço">
        </div>

        <button type="submit" class="btn btn-outline-success">Salvar</button>
    </form>
@endsection

