<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sidebar.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('title')</title>
    <style>

    </style>
    @yield('css')
</head>
<body>
<header class="top">
    <nav class="navbar navbar-dark bg-primary">
        <a class="navbar-brand" href="/">HOME</a>
    </nav>
</header>
<div class="sidebar">
    <a href="/escolas">Escolas</a>
    <a href="/alunos">Alunos</a>
</div>
<div class="content">
    @yield('content')
</div>

</body>
</html>
@yield('script')
