@extends('layouts.main')
@section('title','Turma')

@section('content')
    <h1>Turma: <a href="/escolas/{{$idSchool}}/turmas/form" class="btn btn-outline-success">Cadastrar</a></h1>
    <table class="table table-striped" id="schools">
        <thead class="table-primary">
        <tr>
            <th scope="col">Nome</th>
            <th scope="col">Data de Inicio</th>
            <th scope="col">Data de Final</th>
            <th scope="col">Opções</th>
        </tr>
        </thead>
        <tbody>
        @foreach($schoolClasses as $classe)
            <tr>
                <td>{{$classe->no_curso}}</td>
                <td>{{Carbon\Carbon::createFromDate(Date($classe->dt_inicio))->format('d/m/Y')}}</td>
                <td>{{Carbon\Carbon::createFromDate(Date($classe->dt_final))->format('d/m/Y')}}</td>
                <td>
                    <a href="/escolas/{{$idSchool}}/turmas/{{$classe->id_turma}}/alunos" class="btn btn-outline-primary">Alunos</a>
                    <a href="/escolas/{{$idSchool}}/turmas/{{$classe->id_turma}}/edit" class="btn btn-outline-secondary">Editar</a>
                    <a href="/escolas/{{$idSchool}}/turmas/{{$classe->id_turma}}/delete" class="btn btn-outline-danger">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $schoolClasses->links() ?? ''}}
@endsection
