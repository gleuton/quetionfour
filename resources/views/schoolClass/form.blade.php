@extends('layouts.main')
@section('title','Turma')

@section('content')
    <h1>Turma:</h1>
    <form action="/escolas/{{$idSchool}}/turmas{{$schoolClasse->edit ?? ''}}" method="post">
        @csrf
        <div class="form-group">
            <label for="nocurso">Nome da Turma:</label>
            <input type="text" class="form-control" name="no_curso" value="{{$schoolClasse->no_curso ?? ''}}"
                   autocomplete="off" id="nocurso" placeholder="Turma">
        </div>
        <div class="form-group">
            <label for="inicio">Inicio:</label>
            <input type="date" class="form-control" name="dt_inicio"
                   value="{{isset($schoolClasse->dt_inicio) ? Carbon\Carbon::createFromDate(Date($schoolClasse->dt_inico))->format('Y-m-d') : ''}}"                   autocomplete="off" id="inicio">
        </div>
        <div class="form-group">
            <label for="final">Final:</label>
            <input type="date" class="form-control" name="dt_final"
                   value="{{isset($schoolClasse->dt_final) ? Carbon\Carbon::createFromDate(Date($schoolClasse->dt_final))->format('Y-m-d') : ''}}"
                   autocomplete="off" id="final">
        </div>

        <button type="submit" class="btn btn-outline-success">Salvar</button>
    </form>
@endsection

