@extends('layouts.main')
@section('title','Alunos')

@section('content')
    <h1>Alunos: <a href="/alunos/form" class="btn btn-outline-success">Cadastrar</a></h1>
    <table class="table table-striped" id="schools">
        <thead class="table-primary">
        <tr>
            <th scope="col">Nome</th>
            <th scope="col">Email</th>
            <th scope="col">Telefone</th>
            <th scope="col">Data de Nascimento</th>
            <th scope="col">Opções</th>
        </tr>
        </thead>
        <tbody>
        @foreach($students as $student)
            <tr>
                <td>{{$student->no_aluno}}</td>
                <td>{{$student->email}}</td>
                <td>{{$student->fone}}</td>
                <td>{{\Carbon\Carbon::createFromDate(Date($student->dt_nascimento))->format('d/m/Y')}}</td>
                <td>
                    <a href="/alunos/{{$student->id_aluno}}/matriculas" class="btn btn-outline-primary">Matriculas</a>
                    <a href="/alunos/{{$student->id_aluno}}/edit" class="btn btn-outline-secondary">Editar</a>
                    <a href="/alunos/{{$student->id_aluno}}/delete" class="btn btn-outline-danger">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $students->links() ?? ''}}
@endsection
