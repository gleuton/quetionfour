@extends('layouts.main')
@section('title','Alunos')

@section('content')
    <h1>Alunos:</h1>
    <form action="/alunos{{$student->edit ?? ''}}" method="post">
        @csrf
        <div class="form-group">
            <label for="noAluno">Nome do Aluno:</label>
            <input type="text" class="form-control" name="no_aluno" value="{{$student->no_aluno ?? ''}}"
                   autocomplete="off" id="noAluno" placeholder="Nome do Aluno">
        </div>
        <div class="form-group">
            <label for="email">E-mail:</label>
            <input type="email" class="form-control" name="email" value="{{$student->email ?? ''}}"
                   autocomplete="off" id="email" placeholder="E-mail">
        </div>
        <div class="form-group">
            <label for="fone">Telefone:</label>
            <input type="text" class="form-control" name="fone" value="{{$student->fone ?? ''}}"
                   autocomplete="off" id="fone" placeholder="Telefone">
        </div>
        <div class="form-group">
            <label for="dt_nascimento">Data de Nascimento:</label>
            <input type="date" class="form-control" name="dt_nascimento"
                   value="{{isset($student->dt_nascimento) ? Carbon\Carbon::createFromDate(Date($student->dt_nascimento))->format('Y-m-d') : ''}}"
                   autocomplete="off" id="dt_nascimento">
        </div>
        <button type="submit" class="btn btn-outline-success">Salvar</button>
    </form>
@endsection

