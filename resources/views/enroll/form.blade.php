@extends('layouts.main')
@section('title','Turma')

@section('content')
    <h1>Turma:</h1>
    <form action="/alunos/{{$idStudent}}/matriculas" method="post">
        @csrf
        <div class="form-group">
            <div class="form-group">
                <label for="turma">Esolha uma Turma</label>
                <select class="form-control" id="turma" name="id_turma">
                    @foreach($schoolClasse as $class)
                        <option value="{{$class->id_turma}}">{{$class->no_curso}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-outline-success">Matricular</button>
    </form>
@endsection

