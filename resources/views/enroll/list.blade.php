@extends('layouts.main')
@section('title','Turma')

@section('content')
    <h1>Matricula: <a href="/alunos/{{$idStudent}}/matriculas/form" class="btn btn-outline-success">Matricular</a></h1>
    <table class="table table-striped" id="schools">
        <thead class="table-primary">
        <tr>
            <th scope="col">Nome da Truma</th>
            <th scope="col">Data de Inicio</th>
            <th scope="col">Data de Final</th>
        </tr>
        </thead>
        <tbody>
        @foreach($enrolls as $classes)
            @foreach($classes->schoolClasses as $classe)

                <tr>
                    <td>{{$classe->no_curso}}</td>
                    <td>{{Carbon\Carbon::createFromDate(Date($classe->dt_inicio))->format('d/m/Y')}}</td>
                    <td>{{Carbon\Carbon::createFromDate(Date($classe->dt_final))->format('d/m/Y')}}</td>
                </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>

@endsection
