<?php
/**
 * Author: gleuton
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    private const ID_TURMA = 'id_turma';
    protected $table = 'tb_turma';
    protected $primaryKey = self::ID_TURMA;

    protected $fillable
        = [
            'id_escola',
            'no_curso',
            'dt_inicio',
            'dt_final',
        ];

    protected $dates = ['dt_incio', 'dt_final'];
    public $timestamps = false;

    public function enrolls()
    {
        return $this->hasmany(
            Enroll::class,
            self::ID_TURMA,
            self::ID_TURMA);
    }

}
