<?php
/**
 * Author: gleuton
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    private const PK = 'id_aluno';
    protected $table = 'tb_aluno';
    protected $primaryKey = self::PK;

    protected $fillable
        = [
            'no_aluno',
            'endereco',
            'email',
            'fone',
            'dt_nascimento',
        ];

    protected $dates = ['dt_nascimento'];

    public $timestamps = false;
}
