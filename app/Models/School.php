<?php
/**
 * Author: gleuton
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    private const PK = 'id_escola';
    protected $table = 'tb_escola';
    protected $primaryKey = self::PK;

    protected $fillable
        = [
            'no_escola',
            'endereco',
        ];
    public $timestamps = false;

    public function schoolClasses()
    {
        return $this->hasmany(
            SchoolClass::class,
            self::PK,
            self::PK);
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function (School $school) {
            $school->schoolClasses()->delete();
        });
    }
}
