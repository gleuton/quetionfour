<?php
/**
 * Author: gleuton
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Enroll extends Model
{
    private const ID_TURMA = 'id_turma';
    private const ID_ALUNO = 'id_aluno';

    protected $table = 'rl_matricula';
    protected $primaryKey = [self::ID_TURMA, self::ID_ALUNO];

    protected $fillable = [self::ID_TURMA, self::ID_ALUNO];

    public $timestamps = false;

    public $incrementing = false;

    public function schoolClasses()
    {
        return $this->hasmany(
            SchoolClass::class,
            self::ID_TURMA,
            self::ID_TURMA);
    }

    public function students()
    {
        return $this->hasmany(
            Student::class,
            self::ID_ALUNO,
            self::ID_ALUNO);
    }


}
