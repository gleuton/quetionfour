<?php
/**
 * Author: gleuton
 */

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    const REDIRECT = '/alunos';
    public function index()
    {
        $students = Student::paginate(10);
        return view('student.list')->with('students', $students);
    }

    public function store(Request $request)
    {
        $student = $request->all();
        Student::create($student);
        return redirect(self::REDIRECT);
    }

    public function form(int $idStudent = null)
    {
        if (!isset($idStudent)) {
            return view('student.form');
        }

        $student = Student::findOrFail($idStudent);
        $student->edit = '/' . $student->id_aluno . '/edit';

        return view('student.form')->with('student', $student);
    }

    public function update(Request $request, int $idStudent)
    {
        $datafrom = $request->all();
        $school = Student::findOrFail($idStudent);
        $school->update($datafrom);

        return redirect(self::REDIRECT);
    }

    public function destroy(int $idStudent)
    {
        $school = Student::findOrFail($idStudent);
        $school->delete();
        return redirect(self::REDIRECT);
    }

}
