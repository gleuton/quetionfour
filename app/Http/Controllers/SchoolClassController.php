<?php
/**
 * Author: gleuton
 */

namespace App\Http\Controllers;

use App\Models\SchoolClass;
use Illuminate\Http\Request;

class SchoolClassController extends Controller
{

    public function index(int $idSchool)
    {
        $schoolClasses = SchoolClass::where('id_escola', $idSchool)
            ->paginate(10);

        return view('schoolClass.list')
            ->with('idSchool', $idSchool)
            ->with('schoolClasses', $schoolClasses);
    }

    public function store(Request $request, int $idSchool)
    {
        $schoolClass = $request->all();
        $schoolClass['id_escola'] = $idSchool;
        SchoolClass::create($schoolClass);
        return redirect($this->redirect($idSchool));
    }

    public function form(int $idSchool, int $idClass = null)
    {

        if (!isset($idClass)) {
            return view('schoolClass.form')->with('idSchool', $idSchool);
        }

        $schoolClasse = SchoolClass::findOrFail($idClass);
        $schoolClasse->edit = '/' . $schoolClasse->id_turma . '/edit';

        return view('schoolClass.form')
            ->with('idSchool', $idSchool)
            ->with('schoolClasse', $schoolClasse);
    }

    public function update(Request $request, int $idSchool, int $idSchoolClasse)
    {
        $datafrom = $request->all();
        $school = SchoolClass::findOrFail($idSchoolClasse);

        $school->update($datafrom);

        return redirect($this->redirect($idSchool));
    }
    public function destroy(int $idSchool, int $idSchoolClasse)
    {
        $schoolClasse = SchoolClass::findOrFail($idSchoolClasse);
        $schoolClasse->delete();
        return redirect($this->redirect($idSchool));
    }
    private function redirect(int $idSchool): string
    {
        return "escolas/{$idSchool}/turmas";
    }
}
