<?php
/**
 * Author: gleuton
 */

namespace App\Http\Controllers;

use App\Models\Enroll;
use App\Models\SchoolClass;
use Illuminate\Http\Request;

class EnrollController extends Controller
{

    public function index(int $idStudent)
    {
        $enrolls = Enroll::where('id_aluno', $idStudent)
            ->get();

        return view('enroll.list')
            ->with('idStudent', $idStudent)
            ->with('enrolls', $enrolls);
    }

    public function store(Request $request, int $idSctudent)
    {
        $schoolClass = $request->all();
        $schoolClass['id_aluno'] = $idSctudent;
        Enroll::create($schoolClass);
        return redirect($this->redirect($idSctudent));
    }

    public function form(int $idStudent)
    {
        $schoolClass = SchoolClass::all();

        return view('enroll.form')
            ->with('idStudent', $idStudent)
            ->with('schoolClasse', $schoolClass);


    }

    private function redirect(int $idSctudent): string
    {
        return "/alunos/{$idSctudent}/matriculas";
    }
}
