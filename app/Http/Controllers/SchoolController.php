<?php
/**
 * Author: gleuton
 */

namespace App\Http\Controllers;

use App\Models\School;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    const REDIRECT = '/escolas';

    public function index()
    {
        $schools = School::paginate(10);
        return view('school.list')->with('schools', $schools);
    }

    public function store(Request $request)
    {
        $school = $request->all();
        School::create($school);
        return redirect(self::REDIRECT);
    }

    public function form(int $idSchool = null)
    {
        if (!isset($idSchool)) {
            return view('school.form');
        }

        $school = School::findOrFail($idSchool);
        $school->edit = '/' . $school->id_escola . '/edit';

        return view('school.form')->with('school', $school);
    }

    public function update(Request $request, int $idSchool)
    {
        $datafrom = $request->all();
        $school = School::findOrFail($idSchool);
        $school->update($datafrom);

        return redirect(self::REDIRECT);
    }

    public function destroy(int $idSchool)
    {
        $school = School::findOrFail($idSchool);
        $school->delete();
        return redirect(self::REDIRECT);
    }

}
