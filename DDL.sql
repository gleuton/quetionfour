/* tb_escola */

create table "public"."tb_escola"
(
 "id_escola" int not null generated always as identity (
 minvalue 1
 ),
 "no_escola" varchar(60) not null,
 "endereco"  varchar(150) not null

);

create unique index "pk_tb_escola" on "public"."tb_escola"
(
 "id_escola"
);

/* tb_aluno */

create table "public"."tb_aluno"
(
 "id_aluno"      int not null generated always as identity (
 minvalue 1
 ),
 "no_aluno"      varchar(50) not null,
 "email"         varchar(70) not null,
 "fone"          varchar(14) not null,
 "dt_nascimento" date not null

);

create unique index "pk_tb_aluno" on "public"."tb_aluno"
(
 "id_aluno"
);

/* tb_turma */

create table "public"."tb_turma"
(
 "id_turma"  int not null generated always as identity (
 minvalue 1
 ),
 "id_escola" int not null,
 "no_curso"  varchar(50) not null,
 "dt_inicio" date not null,
 "dt_final"  date not null,
 constraint "fk_35" foreign key ( "id_escola" ) references "public"."tb_escola" ( "id_escola" )
);

create unique index "pk_rl_turma" on "public"."tb_turma"
(
 "id_turma"
);

create index "fkidx_35" on "public"."tb_turma"
(
 "id_escola"
);

/* rl_matricula */

create table "public"."rl_matricula"
(
 "id_turma" int not null,
 "id_aluno" int not null,
 constraint "fk_40" foreign key ( "id_turma" ) references "public"."tb_turma" ( "id_turma" ),
 constraint "fk_43" foreign key ( "id_aluno" ) references "public"."tb_aluno" ( "id_aluno" )
);

create unique index "pk_rl_matricula" on "public"."rl_matricula"
(
 "id_turma",
 "id_aluno"
);

create index "fkidx_40" on "public"."rl_matricula"
(
 "id_turma"
);

create index "fkidx_43" on "public"."rl_matricula"
(
 "id_aluno"
);

/* tb_nota */

create table "public"."tb_nota"
(
 "id_nota"  int not null generated always as identity (
 minvalue 1
 ),
 "vl_nota"  decimal(4,2) not null,
 "id_aluno" int not null,
 constraint "fk_57" foreign key ( "id_aluno" ) references "public"."tb_aluno" ( "id_aluno" )
);

create unique index "pk_tb_nota" on "public"."tb_nota"
(
 "id_nota"
);

create index "fkidx_57" on "public"."tb_nota"
(
 "id_aluno"
);


