<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRlMatriculaTable extends Migration
{
    const ID_TURMA = 'id_turma';
    const ID_ALUNO = 'id_aluno';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rl_matricula', function (Blueprint $table) {
            $table->integer(self::ID_TURMA)->unsigned();
            $table->integer(self::ID_ALUNO)->unsigned();

            $table->primary([self::ID_TURMA, self::ID_ALUNO]);
            $table->foreign(self::ID_TURMA)
                ->references(self::ID_TURMA)
                ->on('tb_turma');

            $table->foreign(self::ID_ALUNO)
                ->references(self::ID_ALUNO)
                ->on('tb_aluno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rl_matricula');
    }
}
