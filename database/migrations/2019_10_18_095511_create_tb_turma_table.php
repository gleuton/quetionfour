<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbTurmaTable extends Migration
{
    const ID_ESCCOLA = 'id_escola';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_turma', function (Blueprint $table) {
            $table->bigIncrements('id_turma');
            $table->integer(self::ID_ESCCOLA)->unsigned();
            $table->string('no_curso', '50');
            $table->date('dt_inicio');
            $table->date('dt_final');

            $table->foreign(self::ID_ESCCOLA)
                ->references(self::ID_ESCCOLA)
                ->on('tb_escola');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_turma');
    }
}
