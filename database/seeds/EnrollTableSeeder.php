<?php

use App\Models\Enroll;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Author: gleuton
 */
class EnrollTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('rl_matricula')->truncate();
        factory(Enroll::class, 15)->create();
    }
}
