<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SchoolTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(SchoolClassTableSeeder::class);
        $this->call(EnrollTableSeeder::class);
    }
}
