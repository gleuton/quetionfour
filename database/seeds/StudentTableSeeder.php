<?php

use App\Models\School;
use App\Models\Student;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Author: gleuton
 */
class StudentTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tb_aluno')->truncate();
        factory(Student::class, 80)->create();
    }
}
