<?php

use App\Models\School;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Author: gleuton
 */
class SchoolTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tb_escola')->truncate();
        factory(School::class, 20)->create();
    }
}
