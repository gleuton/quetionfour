<?php

use App\Models\SchoolClass;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Author: gleuton
 */
class SchoolClassTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tb_turma')->truncate();
        factory(SchoolClass::class, 20)->create();
    }
}
