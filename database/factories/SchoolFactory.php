<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\School;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(School::class, function (Faker $faker) {
    return [
        'no_escola' => 'Escola '.$faker->name,
        'endereco' => $faker->address,
    ];
});
