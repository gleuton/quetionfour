<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Enroll;
use App\Models\SchoolClass;
use App\Models\Student;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Enroll::class, function (Faker $faker) {
    return [
        'id_turma' => $faker->numberBetween(1, SchoolClass::count()),
        'id_aluno' => $faker->numberBetween(1, Student::count()),
    ];
});
