<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\School;
use App\Models\SchoolClass;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(SchoolClass::class, function (Faker $faker) {
    return [
        'id_escola' => $faker->numberBetween(1, School::count()),
        'no_curso' => $faker->words('3',true),
        'dt_inicio' => $faker->dateTimeThisDecade(),
        'dt_final' => $faker->dateTimeThisDecade(),
    ];
});
